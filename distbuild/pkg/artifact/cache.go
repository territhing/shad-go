// +build !solution

package artifact

import (
	"errors"
	"os"
	"path/filepath"
	"sync"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

var (
	ErrNotFound    = errors.New("artifact not found")
	ErrExists      = errors.New("artifact exists")
	ErrWriteLocked = errors.New("artifact is locked for write")
	ErrReadLocked  = errors.New("artifact is locked for read")
)

type entryStatus int

const (
	statusUnlocked entryStatus = iota
	statusRLocked
	statusWLocked
)

type Cache struct {
	rootPath string
	dirs     map[build.ID]*cacheEntry
	mu       *sync.Mutex
}

type cacheEntry struct {
	path   string
	status entryStatus
}

func NewCache(root string) (*Cache, error) {
	err := os.MkdirAll(root, 0644)
	return &Cache{
		rootPath: root,
		dirs:     make(map[build.ID]*cacheEntry),
		mu:       &sync.Mutex{},
	}, err
}

func (c *Cache) Range(artifactFn func(artifact build.ID) error) error {
	c.mu.Lock()
	defer c.mu.Unlock()

	for entry := range c.dirs {
		if err := artifactFn(entry); err != nil {
			return err
		}
	}

	return nil
}

func (c *Cache) Remove(artifact build.ID) (err error) {
	err = nil

	c.mu.Lock()
	defer c.mu.Unlock()

	if entry, ok := c.dirs[artifact]; ok {
		switch entry.status {
		case statusRLocked:
			err = ErrReadLocked
		case statusWLocked:
			err = ErrWriteLocked
		case statusUnlocked:
			os.Remove(entry.path)
		}
	} else {
		err = ErrNotFound
	}

	return
}

func (c *Cache) Create(artifact build.ID) (path string, commit, abort func() error, err error) {
	path = filepath.Join(c.rootPath, artifact.String())
	commit = nil
	abort = nil
	err = nil

	c.mu.Lock()
	defer c.mu.Unlock()

	if entry, ok := c.dirs[artifact]; ok {
		err = ErrWriteLocked
		if entry.status == statusUnlocked {
			err = ErrExists
		}
	} else {
		os.Mkdir(path, 0644)
		c.dirs[artifact] = &cacheEntry{
			path:   path,
			status: statusWLocked,
		}

		done := make(chan struct{})
		commit = func() error {
			select {
			case <-done:
				return nil
			default:
				close(done)
				c.mu.Lock()
				defer c.mu.Unlock()
				c.dirs[artifact].status = statusUnlocked
			}
			return nil
		}
		abort = func() error {
			select {
			case <-done:
				return nil
			default:
				close(done)
				c.mu.Lock()
				defer c.mu.Unlock()
				c.dirs[artifact].status = statusUnlocked
				os.Remove(path)
				delete(c.dirs, artifact)
			}
			return nil
		}
	}

	return
}

func (c *Cache) Get(artifact build.ID) (path string, unlock func(), err error) {
	path = ""
	unlock = nil
	err = nil

	c.mu.Lock()
	defer c.mu.Unlock()

	if entry, ok := c.dirs[artifact]; ok {
		switch entry.status {
		case statusRLocked:
			err = ErrReadLocked
		case statusWLocked:
			err = ErrWriteLocked
		case statusUnlocked:
			path = entry.path
			entry.status = statusRLocked
			unlock = func() {
				c.mu.Lock()
				entry.status = statusUnlocked
				c.mu.Unlock()
			}
		}
	} else {
		err = ErrNotFound
	}

	return
}
