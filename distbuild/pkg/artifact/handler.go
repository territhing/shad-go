// +build !solution

package artifact

import (
	"net/http"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"gitlab.com/slon/shad-go/distbuild/pkg/tarstream"
	"go.uber.org/zap"
)

type Handler struct {
	logger *zap.Logger
	cache  *Cache
}

func NewHandler(l *zap.Logger, c *Cache) *Handler {
	return &Handler{
		logger: l,
		cache:  c,
	}
}

func (h *Handler) Register(mux *http.ServeMux) {
	h.logger.Info("Registering artifact handler")
	mux.HandleFunc("/artifact/", func(w http.ResponseWriter, req *http.Request) {
		h.logger.Info("Handling request")
		rawID := req.URL.Query().Get("id")
		defer req.Body.Close()
		id := build.ID{}
		id.UnmarshalText([]byte(rawID))
		h.logger.Sugar().Infof("Streaming artifact with id=%v", id.String())
		path, unlock, err := h.cache.Get(id)
		defer unlock()

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		} else {
			h.logger.Sugar().Infof("Found artifact with id=%v", id.String())
			w.Header().Set("Content-type", "application/octet-stream")
			w.Header().Set("Content-Encoding", "gzip")
			tarstream.Send(path, w)
		}
	})
}
